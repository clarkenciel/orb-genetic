;;;; genetic.asd

(asdf:defsystem #:genetic
  :description "Describe genetic here"
  :author "Your Name <your.name@example.com>"
  :license "Specify license here"
  :depends-on (#:alexandria
               #:yason
               #:equals
               #:sketch
               #:lparallel)
  :serial t
  :components ((:file "utils")
               (:file "package")
               (:file "graph")
               (:file "geometry")
               (:file "point")
               (:file "performer")
               (:file "layout")
               (:file "layout/graph")
               (:file "search/a-star")
               (:file "graph-search")
               (:file "genetic")))

