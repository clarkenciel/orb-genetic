# genetic

This is a little program that uses a genetic algorithm to generate performer layouts for a performance
piece of mine called "orb."

In the piece, performers sit around a space and have cup-and-wire-telephones strapped to their heads.
Each performer listens to two other performers and speaks to two other performers using the "telephones."
Because of the way the telephones work, the performers have to have fairly specific orientations with
relationship to one another; a performer can only listen to someone who is within roughly 65° of the
first performer's ear and similarly can only speak to someone who is within roughly 65° of their mouth.

Generating these layouts is pretty tedious for me to do by hand, and the layouts I might come up with
are constrained by what I can reason about by "eyeing" the layout. I've started writing this program
to automate as much of the process as possible. The idea here is that each member of a generation
in the genetic algorithm represents a collection of performers (2D positions and orientations) which is
then scored according to how many connections a graph formed from that layout can have. During
reproduction, positions are spliced and potentially mutated. Hopefully, after many generations, the
program will generate a layout that is fully connected.

The end goal of this program is that the layouts will be output as json files that can be read
by other programs that might be better suited to visualization (and/or sonification). Ideally, the
genetic algorithm could also output the best member of each generation so that the process of building
the graph itself could be inspected/visualized.
