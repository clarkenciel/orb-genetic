(in-package #:cl-user)
(defpackage #:genetic.geometry
  (:use #:cl))
(in-package #:genetic.geometry)

(defgeneric angle-between (x y)
  (:documentation "Return the angle, in degrees between X and Y."))

(defgeneric distance (x y)
  (:documentation "Return the distance between X and Y."))
