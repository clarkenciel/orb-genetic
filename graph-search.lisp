(in-package #:cl-user)
(defpackage #:genetic.graph-search
  (:use #:cl
        #:equals
        #:genetic.graph
        #:genetic.search.a*
        #:genetic.layout.graph)
  (:export #:graph-a*))
(in-package #:genetic.graph-search)

(defclass graph-search-step ()
  ((graph :initarg :graph :reader search-step-graph)
   (layout :initarg :layout :reader search-step-layout)
   (newest-member :initarg :newest-member :reader search-step-newest-member)))

(defun possible-listeners (performer performers graph)
  (when (can-speak-in? graph performer)
    (remove-if
     #'(lambda (other)
         (or (equals performer other)
            (not (can-listen-in? graph other))
            (not (can-connect-in? performers graph performer other))))
     performers)))

(defun possible-graphs (performer performers graph)
  (let ((graphs '()))
    (alexandria:map-permutations
     #'(lambda (permutation)
         (pushnew
          (cons permutation
                (add-nodes performer permutation graph))
          graphs :test #'equals))
     (possible-listeners performer performers graph))
    graphs))

(defmethod frontier-of ((search-step graph-search-step))
  (with-slots (graph layout newest-member) search-step
    (possible-graphs newest-member layout graph)))

(defmethod a*-distance ((search-step graph-search-step) &optional goal)
  (declare (ignore goal))
  (with-slots (graph layout) search-step
    (let ((edge-count (edge-count graph))
          (node-count (node-count graph))
          (performer-count (length layout)))
      (/ (- performer-count edge-count)
         node-count))))

(defun graph-a* (layout)
  (a* (make-instance 'graph-search-step
                     :graph (graph-with)
                     :layout layout
                     :newest-member (first layout))))
