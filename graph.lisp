(in-package #:cl-user)
(defpackage #:genetic.graph
  (:use #:cl)
  (:export #:add-node
           #:node-with
           #:from-node
           #:to-node
           #:involves-node
           #:edge->
           #:edge<->
           #:graph-with
           #:add-nodes
           #:remove-node
           #:edges-matching
           #:edges-involving
           #:edges-from
           #:edges-to
           #:edge-count
           #:node-count))
(in-package #:genetic.graph)

(defclass node ()
  ((value :initarg :value :reader node-value)))

(defun node-with (value)
  (make-instance 'node :value value))

(defclass edge ()
  ((a :initarg :a :accessor edge-a)
   (b :initarg :b :accessor edge-b)))

(defun from-node (node edge)
  (equals node (edge-a edge)))

(defun to-node (node edge)
  (equals node (edge-b edge)))

(defun involves-node (node edge)
  (or (from-node node edge) (to-node node edge)))

(defmethod equals ((edge-one edge) (edge-two edge) &rest keys &key recursive)
  (declare (ignore recursive keys))
  nil)

(defclass directed-edge (edge) ())

(defun edge-> (from to)
  (make-instance 'directed-edge :a from :b to))

(defmethod equals ((edge-one directed-edge) (edge-two directed-edge) &rest keys &key recursive)
  (declare (ignore recursive keys))
  (with-slots ((a1 a) (b1 b)) edge-one
    (with-slots ((a2 a) (b2 b)) edge-two
      (and (equals a1 a2) (equals b1 b2)))))

(defclass undirected-edge (edge) ())

(defun edge<-> (a b)
  (make-instance 'undundirected-edge :a a :b b))

(defmethod equals ((edge-one undirected-edge) (edge-two undirected-edge) &rest keys &key recursive)
  (declare (ignore recursive keys))
  (with-slots ((a1 a) (b1 b)) edge-one
    (with-slots ((a2 a) (b2 b)) edge-two
      (and (equals a1 a2) (equals b1 b2)))))

(defclass graph ()
  ((nodes :initarg :nodes :initform '() :accessor graph-nodes)
   (edges :initarg :edges :initform '() :accessor graph-edges)))

(defun graph-with (&key (nodes '()) (edges '()))
  (make-instance 'graph :nodes nodes :edges edges))

(defun node- (graph-one graph-two)
  (set-difference
   (graph-nodes graph-one)
   (graph-nodes graph-two)
   :test #'equals))

(defmacro assuming-graph-membership (node graph &body body)
  `(if (not (member ,node (graph-nodes ,graph) :test #'equals))
       ,graph
       ,@body))

(defun add-node (parent-node new-node graph &key (edge-type 'directed-edge))
  (assuming-graph-membership parent-node graph
    (let ((new-edge (make-instance edge-type :a parent-node :b new-node)))
      (graph-with
       :nodes (adjoin new-node (graph-nodes graph))
       :edges (adjoin new-edge (graph-edges graph))))))

(defun add-nodes (parent-node new-nodes graph &key (edge-type 'directed-edge))
  (if (null new-nodes) graph
      (add-nodes parent-node
                 (cdr new-nodes)
                 (add-node parent-node (car new-nodes) graph)
                 :edge-type edge-type)))

(defun remove-node (node graph)
  (assuming-graph-membership node graph
    (graph-with
     :nodes (remove node (graph-nodes graph) :test #'equals)
     :edges (remove-if (partial #'involves-node node)
                       (graph-edges graph)))))

(defmacro assuming-list-membership (item list &body body)
  `(if (not (member ,item ,list :test #'equals))
       '()
       ,@body))

(defun edges-matching (predicate graph)
  (remove-if-not predicate (graph-edges graph)))

(defun edges-involving (node graph)
  (assuming-list-membership node (graph-nodes graph)
    (edges-matching (partial #'involves-node node) graph)))

(defun edges-from (node graph)
  (assuming-list-membership node (graph-nodes graph)
    (edges-matching (partial #'from-node node) graph)))

(defun edges-to (node graph)
  (assuming-list-membership node (graph-nodes graph)
    (edges-matching (partial #'to-node node) graph)))

(defun edge-count (graph)
  (length (graph-edges graph)))

(defun node-count (graph)
  (length (grpah-nodes graph)))
