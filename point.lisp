(in-package #:cl-user)
(defpackage #:genetic.point
  (:use #:cl #:genetic.geometry))
(in-package #:genetic.point)

(defclass point ()
  ((x :type double :initarg :x :initform 0 :accessor point-x)
   (y :type double :initarg :y :initform 0 :accessor point-y)))

(defun mid-point (point-one point-two)
  (let ((mid-x (/ (+ (point-x point-one) (point-x point-two))
                  2.0))
        (mid-y (/ (+ (point-y point-one) (point-y point-two))
                  2.0)))
    (point-at mid-x mid-y)))

(defun point-at (x y)
  (make-instance 'point :x x :y y))

(defmethod equals ((x point) (y point) &rest keys &key recursive)
  (declare (ignore keys recursive))
  (with-slots ((x1 x) (y1 y)) x
    (with-slots ((x2 x) (y2 y)) y
      (and (= x1 x2) (= y1 y2)))))

(defmethod distance ((x point) (y point))
  (with-slots ((x1 x) (y1 y)) x
    (with-slots ((x2 x) (y2 y)) y
      (sqrt (+ (expt (- x2 x1) 2)
            (expt (- y2 y1) 2))))))

(defun rad->deg (x)
  (* x (/ 180 pi)))

(defun deg->rad (x)
  (* pi (/ x 180)))

(defmethod angle-between ((x point) (y point))
  (with-slots ((x1 x) (y1 y)) x
    (with-slots ((x2 x) (y2 y)) y
      (let ((angle (rad->deg (atan (- y2 y1) (- x2 x1)))))
        (mod (+ 360 angle) 360)))))
