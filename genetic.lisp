;;;; genetic.lisp
(in-package #:cl-user)
(defpackage #:genetic
  (:use #:cl
        #:equals
        #:sketch
        #:lparallel
        #:genetic.graph
        #:genetic.graph-search
        #:genetic.layout))
(in-package #:genetic)

(defun sort-layouts (population)
  (nreverse (sort population #'< :key #'score-layout)))

(defun best-layout (population)
  (max-by population :key #'score-layout))

(defun random-population (population-size number-of-performers)
  (loop for x from 1 upto population-size
        collect (random-layout number-of-performers)))

(defun adjust-position (performer)
  (with-slots (location orientation) performer
    (performer-at
     (alexandria:clamp
      (+ (point-x location) (rand-range -0.01 0.01))
      *stage-min* *stage-max*)
     (alexandria:clamp
      (+ (point-y location) (rand-range -0.01 0.01))
      *stage-min* *stage-max*)
     :orientation orientation)))

(defun adjust-orientation (performer)
  (with-slots (location orientation) performer
    (performer-at (point-x location)
                  (point-y location)
                  :orientation
                  (mod (+ orientation
                          (rand-range
                           (- *performer-speaking-angle*)
                           *performer-speaking-angle*
                           ;;-60.0 60.0
                           ))
                       360))))

(defun adjust-all (performer)
  (adjust-orientation (adjust-position performer)))

(defun possible-connections (layout)
  (flet ((possible-outputs (performer)
           (mapcar (partial 'could-connect? performer)
                   (remove performer layout :test 'equals))))
    (pmapcar #'possible-outputs layout)))

(defun score-layout (layout)
  (let* ((num-performers (length layout))
         (max-possible-connections (* num-performers (1- num-performers))))
    (/ (length (possible-connections layout))
       max-possible-connections))
  ;; (let ((graph (build-graph layout)))
  ;;   (grade-graph layout graph))
  )

(defun crossover-ratios (&rest layouts)
  (let* ((scores (loop for layout in layouts collect (score-layout layout)))
         (total-score (reduce #'+ scores)))
    (apply #'values
           (loop for score in scores collect
                (if (= 0 total-score)
                    (/ 1.0 (length layouts))
                    (/ score total-score))))))

(defun clone-layout (layout)
  (copy-list layout))

(defun single-insertion-crossover (layout-one layout-two)
  (multiple-value-bind (first-ratio second-ratio) (crossover-ratios layout-one layout-two)
    (let* ((first-part-size (ceiling (* first-ratio (length layout-one))))
           (first-part (clone-layout (take first-part-size layout-one)))
           (second-part-size (floor (* second-ratio (length layout-two))))
           (second-part (clone-layout (take second-part-size layout-two))))
      (append first-part second-part))))

(defun average-crossover (layout-one layout-two)
  (flet ((average-performer (performer-one performer-two)
           (let ((new-loc (mid-point (performer-point performer-one)
                                     (performer-point performer-two))))
             (with-slots (x y) new-loc
               (performer-at x y :orientation (/ (+ (performer-orientation performer-one)
                                                    (performer-orientation performer-two))
                                                 2.0))))))
    (mapcar #'average-performer layout-one layout-two)))

(defun crossover (layout-one layout-two)
  (average-crossover layout-one layout-two))

(defun mutate (layout)
  (loop for performer in layout
     collect (adjust-orientation performer)))

(defun maybe-mutate (layout)
  (if (>= 0.2 (random 1.0))
      (mutate layout)
      (clone-layout layout)))

(defun maybe-crossover (layout-one layout-two)
  (if (>= 0.5 (random 1.0))
      (crossover layout-one layout-two)
      (first
       (sort (list layout-one layout-two)
             #'(lambda (x y)
                 (>= (score-layout x)
                    (score-layout y)))))))

(defun breed (layout-one layout-two)
  (maybe-mutate (maybe-crossover layout-one layout-two)))

(defun breed-population (population-size breeding-pool)
  (cons
   (best-layout breeding-pool)
   (loop
      for x from 1 upto (1- population-size)
      collect (let* ((mate-one (random-choice breeding-pool))
                     (other-mates (remove mate-one breeding-pool :test #'equals)))
                (if (null other-mates)
                    (maybe-mutate mate-one)
                    (breed mate-one (random-choice other-mates)))))))

(defun score-population (population)
  (float (score-layout (best-layout population))))

(defun evolve-step (population breeding-pool-size)
  (let* ((sorted (sort-layouts population))
         (breeding-pool (take breeding-pool-size sorted)))
    (breed-population (length sorted) breeding-pool)))

(defun evolve-layout
    (number-of-performers
     &key (limit 10e3) (population-size 100) (breeding-pool-size 10) callback)
  (loop
     for step from 1 upto limit
     for population = (random-population population-size number-of-performers)
         then (evolve-step population breeding-pool-size)
     do (when callback (funcall callback step (copy-list population)))
     when (= step limit) return (first (sort-layouts population))))

(defun print-layout (layout)
  (with-open-stream (string (make-string-output-stream))
    (format string "LAYOUT:~%")
    (loop for performer in layout
       do (with-slots (location orientation) performer
            (format string "  <PERFORMER: (~,5f, ~,5f) ~,5f>~%"
                    (point-x location)
                    (point-y location)
                    orientation)))
    (get-output-stream-string string)))

(defun evolve-with-logging (&rest arguments)
  (flet ((log-score (step population)
           (let* ((population (sort-layouts population))
                  (best-layout (first population))
                  (score (grade-graph best-layout (build-graph best-layout))))
             (format t "step ~a: ~a~%"
                     step score)
             ;; (format t "step ~a: ~a~%~{  ~a~%~}"
             ;;         step score (mapcar #'print-layout population))
             )))
    (apply #'evolve-layout
           (append arguments (list :callback #'log-score)))))

(defparameter *stage-width* 800)

(defparameter *stage-height* 800)

(defun draw-performer (performer)
  (with-slots (location orientation) performer
    (sketch:rotate orientation (point-x location) (point-y location))
    (sketch:ngon 3 (+ 10 (point-x location)) (point-y location) 10 5)
    (sketch:ellipse (point-x location) (point-y location) 10 10)
    (sketch:rotate (- orientation) (point-x location) (point-y location))))

(defun scale-performer (performer)
  (with-slots (location orientation) performer
    (let ((stage-x (* *stage-width* (point-x location)))
          (stage-y (* *stage-height* (point-y location))))
      (performer-at stage-x stage-y :orientation orientation))))

(defun scale-layout (layout)
  (loop for performer in layout
       collect (scale-performer performer)))

(defun draw-layout-stats (layout)
  (with-pen (make-pen :fill (rgb 1 1 1) :stroke (rgb 1 1 1) :weight 0)
    (text (format nil "SCORE: ~,5f~% ~s" (score-layout layout) (print-layout layout))
          10 10)))

(defun draw-layout (layout)
  (let ((scaled (scale-layout layout)))
    (loop for performer in scaled
       do (draw-performer performer))
    (draw-layout-stats scaled)))

;; NB: set environment var SDL_VIDEODRIVER=wayland for personal laptop
(defsketch evolve-sketch
    ((title "evolve")
     (population (random-population 200 6))
     (pool-size 10)
     (height *stage-height*)
     (width *stage-width*))
  (let ((best-layout (best-layout population)))
    (when (< (score-layout best-layout) 1)
      (setf population (evolve-step population pool-size)))
    (with-pen (make-pen :fill (rgb 0 0 0 0.1))
      (rect 0 0 *stage-width* *stage-height*))
    (draw-layout best-layout)))

(defmethod kit.sdl2:mousebutton-event ((instance evolve-sketch) state timestamp button x y)
  (when (equal state :mousebuttondown)
    (with-slots (population pool-size) instance
      (setf population (evolve-step population pool-size)))))

(defun evolve-with-display ()
  (make-instance 'evolve-sketch))
