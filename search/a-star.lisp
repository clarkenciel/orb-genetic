(in-package #:cl-user)
(defpackage #:genetic.search.a*
  (:use #:cl #:equals #:genetic.utils)
  (:export :a*
           :a*-distance
           :frontier-of))
(in-package #:genetic.search.a*)

(defgeneric a*-distance (object &optional goal)
  (:documentation "Provide an orderable score for an element searched in an a* search."))

(defstruct a*-step
  candidate
  distance
  path)

(defun construct-step (goal current-distance path new-candidate)
  (make-a*-step
    :candidate new-candidate
    :distance (+ current-distance (a*-distance new-candidate goal))
    :path path))

(defun a*-frontier (current-distance goal path-here)
  (sort (mapcar (partial #'construct-step goal current-distance path-here)
                (frontier-of (car path-here)))
        #'< :key #'a*-step-distance))

(defun merge-frontiers (new-frontier old-frontier)
  (loop for candidate in new-frontier
        do (pushnew candidate old-frontier :test #'equals))
  (sort old-frontier #'< :key #'cdr))

;;; TODO: integrate construct-step into the following functions
(defun go-forth (current-cost goal frontier visited candidate candidate-cost)
  (format t "~a is ~a from ~a~%" candidate candidate-cost goal)
  (if (= 0 candidate-cost) (nreverse (pushnew candidate visited :test #'equals))
    (let* ((this-cost (+ current-cost candidate-cost))
           (this-frontier (a*-frontier this-cost goal candidate))
           (next-frontier (merge-frontiers this-frontier frontier))
           (new-visted (copy-list visited)))
      (and next-frontier
           (go-forth
             this-cost
             goal
             (cdr next-frontier)
             (pushnew candidate new-visted :test #'equals)
             (car (car next-frontier))
             (cdr (car next-frontier)))))))

(defun explore (candidate &optional goal)
  (let ((this-cost (a*-distance candidate goal)))
    (go-forth 0 goal '() '() candidate this-cost)))

(defun a* (start &optional goal)
  (explore start goal))

