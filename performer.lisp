(in-package #:cl-user)
(defpackage #:genetic.performer
  (:use #:cl
        #:genetic.point
        #:genetic.geometry))
(in-package #:genetic.performer)

(defparameter *listening-angle* 90)

(defparameter *speaking-angle* 90)

(defparameter *max-inputs* 2)

(defparameter *max-outputs* 2)

(defparameter *stage-min* 0.01)

(defparameter *stage-max* 1.0)

(defclass performer ()
  ((location :type point :initarg :location :initform (point-at 0 0) :reader performer-point)
   (orientation :type double :initarg :orientation :initform 0 :reader performer-orientation)))

(defun performer-at (x y &rest attributes)
  (apply #'make-instance 'performer :location (point-at x y) attributes))

(defun clone-performer (performer)
  (with-slots (location orientation) performer
    (performer-at (point-x location) (point-y location) :orientation orientation)))

(defmethod distance ((x performer) (y performer))
  (distance (performer-point x) (performer-point y)))

(defmethod angle-between ((x performer) (y performer))
  (angle-between (performer-point x) (performer-point y)))

(defmethod equals ((x performer) (y performer) &rest keys &key recursive)
  (declare (ignore keys recursive))
  (and (equals (performer-point x) (performer-point y))
     (equals (performer-orientation x) (performer-orientation y))))

(defun speaking-angles (performer)
  (with-slots (orientation) performer
    (list
     (+ orientation *speaking-angle*)
     (+ orientation (- *speaking-angle*)))))

(defun maximum (values)
  (apply #'max values))

(defun minimum (values)
  (apply #'min values))

(defun within-speaking-angle? (target source)
  (let ((speaking-angles  (speaking-angles source)))
    (<= (minimum speaking-angles)
       (angle-between source target)
       (maximum speaking-angles))))

(defun left-listening-angles (performer)
  (with-slots (orientation) performer
    (list
     (+ orientation -90 *listening-angle*)
     (+ orientation -90 (- *listening-angle*)))))

(defun right-listening-angles (performer)
  (with-slots (orientation) performer
    (list
     (+ orientation 90 *listening-angle*)
     (+ orientation 90 (- *listening-angle*)))))

(defun within-left-listening-angle? (source target)
  (let ((listening-angles (left-listening-angles target)))
    (<= (minimum listening-angles)
       (angle-between target source)
       (maximum listening-angles))))

(defun within-right-listening-angle? (source target)
  (let ((listening-angles (right-listening-angles target)))
    (<= (minimum listening-angles)
       (angle-between target source)
       (maximum listening-angles))))

(defun within-listening-angle? (source target)
  (or (within-left-listening-angle? source target)
     (within-right-listening-angle? source target)))

(defun max-inputs (performer)
  *max-inputs*)

(defun max-outputs (performer)
  *max-outputs*)

(defun could-connect? (source target)
  (and (within-listening-angle? source target)
     (within-speaking-angle? target source)))

(defun blocked-by? (source target performer)
  (let ((angle (angle-between source target))
        (distance (distance source target))
        (other-angle (angle-between source performer))
        (other-distance (distance source performer)))
    (and (= (round other-angle) (round angle))
       (<= other-distance distance))))
