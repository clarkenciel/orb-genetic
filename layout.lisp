(in-package #:cl-user)
(defpackage #:genetic.layout
  (:use #:cl
        #:genetic.performer))
(in-package #:genetic.layout)

(defun blocked? (source target performers)
  (let ((performers-to-check (set-difference performers (list source target))))
    (find-if (partial #'blocked-by? source target)
             performers-to-check)))

(defun position-free? (performer performer-set)
  (not (member performer performer-set
             :test #'(lambda (p1 p2)
                       (equals (performer-point p1) (performer-point p2))))))

(defun random-layout (performer-count)
  (let ((x-range (list *stage-min* *stage-max*))
        (y-range (list *stage-min* *stage-max*)))
    (labels ((build (performer-set performer-count)
               (if (= 0 performer-count) performer-set
                   (let ((new-performer (performer-at (apply #'rand-range x-range)
                                                      (apply #'rand-range y-range)
                                                      :orientation (rand-range 0 360))))
                     (if (position-free? new-performer performer-set)
                         (build (adjoin new-performer performer-set :test #'equals)
                                (1- performer-count))
                         (build performer-set performer-count))))))
      (build '() performer-count))))
