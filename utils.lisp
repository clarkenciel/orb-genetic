(in-package #:cl-user)
(defpackage #:genetic.utils
  (:use :cl)
  (:export :partial
           :take
           :random-choice
           :rand-range
           :max-by))
(in-package #:genetic.utils)

(defun partial (function &rest first-args)
  (lambda (&rest other-args)
    (apply function (append first-args other-args))))

(defun take (count seq)
  (loop for x from 0 upto count for element in seq
     until (= x count)
     collect element))

(defun random-choice (collection)
  (let ((size (length collection)))
    (nth (random size) collection)))

(defun rand-range (lo hi)
  (+ lo (random (- hi lo))))

(defun max-by (collection &key key)
  (let ((backup #'(lambda (x) x)))
    (loop
       for item in collection
       for max = (first collection)
       then (if (> (funcall (or key backup) item) (funcall (or key backup) max))
                item
                max)
       do (setf max item)
       finally (return max))))
