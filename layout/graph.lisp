(in-package #:cl-user)
(defpackage #:genetic.layout.graph
  (:use #:cl #:genetic.graph #:genetic.layout #:genetic.performer)
  (:export #:can-listen-in?
           #:can-speak-in?
           #:can-connect-in?))
(in-package #:genetic.layout.graph)

(defun can-listen-in? (graph performer)
  (> (max-inputs performer)
     (length (edges-to performer graph))))

(defun can-speak-in? (graph performer)
  (> (max-outputs performer)
     (length (edges-from performer graph))))

(defun can-connect-in? (performers graph source target)
  (and (not (blocked? source target performers))
     (could-connect? target source)
     (can-speak-in? graph source)
     (can-listen-in? graph target)))
